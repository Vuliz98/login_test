import 'package:get/get.dart';

import 'package:login_test/app/modules/home/bingdings/home_bingdings.dart';
import 'package:login_test/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
  ];
}
